import tdsc


class GnomeShellPackage(tdsc.apt.InstallAptPackages):
    """Ensure the Gnome Session package is installed on this system."""
    name = "Install gnome-session"
    packages = ['gnome-session']
    tags = [set(['ubuntu'])]


class RemoveUbuntuShellPackage(tdsc.apt.RemoveAptPackages):
    """Ensure the Ubuntu Shell package is not installed on this system."""
    name = "Remove ubuntu-session"
    packages = ['ubuntu-session']
    tags = [set(['ubuntu'])]


states = [
    GnomeShellPackage(),
    RemoveUbuntuShellPackage()
]
