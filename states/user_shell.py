import tdsc


class AptZshState(tdsc.apt.InstallAptPackages):
    """Ensure preferred shells are installed."""
    name = 'Install bash and zsh'
    packages = ['bash', 'zsh']


states = [
    AptZshState()
]
