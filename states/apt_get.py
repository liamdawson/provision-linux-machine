import tdsc
import platform


class UbuntuAptMirrors(tdsc.primatives.TemplateState):
    """Ensure the preferred Ubuntu apt mirror is used."""
    name = "Select preferred APT mirror"
    tags = [set(['ubuntu'])]
    destination = "/etc/apt/sources.list"
    template = """
# set by provision.py

deb {mirror} {codename} main restricted universe multiverse
deb {mirror} {codename}-security main restricted universe multiverse
deb {mirror} {codename}-updates main restricted universe multiverse
deb {mirror} {codename}-backports main restricted universe multiverse
    """

    @property
    def args(self):
        return {
            'mirror': 'http://mirror.as24220.net/pub/ubuntu/',
            'codename': platform.linux_distribution()[2]
        }


class EnsureAptResources(tdsc.apt.InstallAptPackages):
    """Ensure the apt https and gpg are available."""
    name = 'Install https apt transport, gpg'
    tags = [set(['ubuntu']), set(['debian'])]
    packages = [
        'apt-transport-https',
        'gpg'
    ]


states = [
    UbuntuAptMirrors(),
    tdsc.apt.UpdateAptPackageList(),
    tdsc.apt.UpdateAptPackages(),
    EnsureAptResources()
]
