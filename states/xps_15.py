import tdsc


class ProprietaryNvidiaGraphics(tdsc.apt.InstallAptPackages):
    """Don't install any Nvidia graphics, because any combination I've tried makes my computer unhappy."""
    name = "Install nvidia-driver-390"
    tags = [set(['ubuntu', 'xps15', 'OR NOT'])]
    packages = [
        'nvidia-driver-390'
    ]


class SetGrubDefaultTemplate(tdsc.primatives.TemplateState):
    """Set workaround parameters for device in bootloader."""
    tags = [set(['ubuntu', 'xps15'])]
    template = """
GRUB_DEFAULT=saved
GRUB_SAVEDEFAULT=true
GRUB_HIDDEN_TIMEOUT_QUIET=true
GRUB_TIMEOUT=5
GRUB_DISTRIBUTOR=`lsb_release -i -s 2> /dev/null || echo Debian`
GRUB_CMDLINE_LINUX_DEFAULT="quiet splash"
GRUB_CMDLINE_LINUX="nouveau.modeset=0 acpi_rev_override=5 nvme_core.default_ps_max_latency_us=180000 iwlwifi.power_save=1 video=1920x1080"
GRUB_GFXMODE=1920x1440x32
"""
    args = {}
    destination = '/etc/default/grub'

class UpdateGrub(tdsc.primatives.CommandState):
    """Update grub bootloader configuration."""
    tags = [set(['ubuntu', 'xps15'])]
    command = ['update-grub']

states = [
    ProprietaryNvidiaGraphics(),
    SetGrubDefaultTemplate(),
    UpdateGrub()
]
