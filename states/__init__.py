from . import gnome_shell, apt_get, xps_15, user_shell, ubuntu, microsoft

states = [] + apt_get.states + gnome_shell.states + user_shell.states + ubuntu.states + microsoft.states + xps_15.states

def relevant_states(context):
  return filter(lambda state: state.should_apply(context), states)
