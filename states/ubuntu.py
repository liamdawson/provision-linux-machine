import tdsc

class UbuntuDevPackages(tdsc.apt.InstallAptPackages):
  """Ensure preferred dev packages are installed for general dev tasks."""
  name = "Install dev packages"
  packages = [
    'stow',
    'vim',
    'git',
    'build-essential',
    'w3m',
    'cmake',
    'gdb',
    'libz-dev',
    'libffi-dev',
    'libssl-dev',
    'libbz2-dev',
    'libreadline-dev',
    'libsqlite3-dev',
    'llvm',
    'libncurses5-dev',
    'libncursesw5-dev',
    'xz-utils',
    'tk-dev'
  ]
  tags = [set(['ubuntu'])]

class UbuntuVanityPackages(tdsc.apt.InstallAptPackages):
  """Ensure preferred vanity packages are installed for general use."""
  name = "Install vanity packages"
  packages = [
    'fonts-roboto',
    'fonts-noto',
    'arc-theme'
  ]
  tags = [set(['ubuntu'])]
 
states = [
  UbuntuDevPackages(),
  UbuntuVanityPackages()
]
