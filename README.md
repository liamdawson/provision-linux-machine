# *nix Machine Provisioning

License: [CC0-1.0](LICENSE)

Repo: https://gitlab.com/liamdawson/provision-linux-machine

Liam Dawson's personal *nix machine provisioning scripts.

## Instructions

### Ubuntu Dell XPS 15

#### After new install

```shell
# enable network, then...

sudo apt-get install -y curl git
git clone https://gitlab.com/liamdawson/provision-linux-machine.git ~/provision --recurse-submodules
cd ~/provision
# preview states to apply, generate bytecode as user
python3 provision.py ubuntu xps15
# apply states
sudo !! --apply
```
